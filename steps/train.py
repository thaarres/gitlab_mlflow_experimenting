from typing import Dict, Any
import os
import numpy as np
import logging
logger = logging.getLogger(__name__)

def get_model():
  """
  This should be more configurable
  """
  from tensorflow.keras.models import Sequential
  from tensorflow.keras.layers import Dense, Activation, BatchNormalization
  from tensorflow.keras.optimizers import Adam
  from tensorflow.keras.regularizers import l1
  import mlflow

  model = Sequential()
  model.add(Dense(64, input_shape=(16,), name='fc1', kernel_initializer='lecun_uniform', kernel_regularizer=l1(0.0001)))
  model.add(Activation(activation='relu', name='relu1'))
  model.add(Dense(32, name='fc2', kernel_initializer='lecun_uniform', kernel_regularizer=l1(0.0001)))
  model.add(Activation(activation='relu', name='relu2'))
  model.add(Dense(32, name='fc3', kernel_initializer='lecun_uniform', kernel_regularizer=l1(0.0001)))
  model.add(Activation(activation='relu', name='relu3'))
  model.add(Dense(5, name='output', kernel_initializer='lecun_uniform', kernel_regularizer=l1(0.0001)))
  model.add(Activation(activation='softmax', name='softmax'))
  adam = Adam(lr=0.0001)
  model.compile(optimizer=adam, loss=['categorical_crossentropy'], metrics=['accuracy'])
  return model

def train(model, X, y, fitkwargs, save=False, workdir='./'):
  import mlflow
  mlflow.set_experiment("hls4ml tutorial")
  with mlflow.start_run(run_name="hls4ml_tutorial_1") as run:
    mlflow.tensorflow.autolog()
    model.fit(X, y, **fitkwargs)
    if os.getenv('GITLAB_CI'):
      mlflow.set_tag('gitlab.CI_JOB_ID', os.getenv('CI_JOB_ID'))
    if save:
      if not os.path.exists(f'{workdir}/model'):
        os.makedirs(f'{workdir}/model')
      model.save(f'{workdir}/model/model.keras')
    return model

def load_train_data(workdir='./'):
  X_train = np.load(f'{workdir}/data/train/X_transformed.npy')
  y_train = np.load(f'{workdir}/data/train/y.npy')
  return X_train, y_train

if __name__ == '__main__':
  from args import get_common_parser, handle_common_args
  parser = get_common_parser()
  parser.add_argument('--train-batch-size', dest='batch_size', default=1024)
  parser.add_argument('--train-epochs', dest='epochs', default=25)
  parser.add_argument('--train-validation-split', dest='validation_split', default=.25)
  parser.add_argument('--train-shuffle', dest='shuffle', default=True)
  args = parser.parse_args()
  handle_common_args(args)
  logger.setLevel(logging.DEBUG)
  X_train, y_train = load_train_data(args.workdir)
  model = get_model()
  train(model,
        X_train,
        y_train,
        {'batch_size' : args.batch_size,
         'epochs'     : args.epochs,
         'validation_split' : args.validation_split,
         'shuffle' : args.shuffle },
        args.save,
        args.workdir
        )